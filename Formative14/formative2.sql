USE Staging;

CREATE TABLE Product_ClientA(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    description VARCHAR(20) NOT NULL,
    artNumber INT NOT NULL,
    price INT NOT NULL,
    stock INT NOT NULL,
    deleted boolean NOT NULL,
    brand_name VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)  
);

CREATE TABLE Product_ClientB(
    id INT NOT NULL AUTO_INCREMENT,
    nama VARCHAR(20) NOT NULL,
    deskripsi VARCHAR(20) NOT NULL,
    nomor_artikel INT NOT NULL,
    harga INT NOT NULL,
    persediaan_barang INT NOT NULL,
    dihapus boolean NOT NULL,
    nama_merek VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)  
);

USE Production;


CREATE TABLE Product(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    description VARCHAR(20) NOT NULL,
    art_number INT NOT NULL,
    price INT NOT NULL,
    stock INT NOT NULL,
    is_deleted boolean NOT NULL,
    brand VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)  
);