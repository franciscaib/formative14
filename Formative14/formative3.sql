USE Staging;

INSERT INTO Product_ClientA(name, description, artNumber, price, stock, deleted, brand_name) VALUES
('Pensil','Alat tulis', 1, 1000, 50, False, 'Faber Castel'),
('Buku','Alat tulis', 2, 3000, 70, False, 'Sinar Dunia'),
('Pensil Warna','Alat tulis', 3, 3000, 5, False, 'Luna'),
('Sabun Mandi','Alat kebersihan', 4, 15000, 40, False, 'Detol'),
('Pasta gigi','Alat kebersihan', 5, 10000, 20, False, 'Pepsodent'),
('Air mineral','Minuman', 6, 4000, 100, False, 'Aqua'),
('Minuman soda','Minuman', 7, 7000, 30, False, 'Sprite'),
('Sampo','Alat Kebersihan', 8, 11000, 18, False, 'Pantene'),
('Penghapus','Alat tulis', 9, 1000, 50, False, 'Standart'),
('Sabun cuci piring','Alat kebersihan', 10, 17000, 50, False, 'Sunlight');


INSERT INTO Product_ClientB(nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek) VALUES
('Permen','Makanan ringan', 11, 1000, 500, False, 'Fox'),
('Wafer','Makanan ringan', 12, 12000, 80, False, 'Tango'),
('Cokelat','Makanan ringan', 13, 13000, 45, False, 'Silver Queen'),
('Biskuit','Makanan ringan', 14, 4000, 49, False, 'Biskuat'),
('Mie Instan','Makanan instan', 15, 5000, 20, False, 'Sedap'),
('Tepung','Tepung instan', 16, 14000, 100, False, 'Segitiga Biru'),
('Gas','Alat rumah tangga', 17, 27000, 30, False, 'Gas LPG'),
('Obat flu','Obat ringan', 18, 1000, 18, False, 'Influenza'),
('Penutup luka','Obat ringan', 19, 1000, 50, False, 'Handsaplast'),
('Detergen','Alat kebersihan', 20, 30000, 50, False, 'Boom');