USE Production;

INSERT INTO Product(name, description, art_number, price, stock, is_deleted, brand)
	SELECT Staging.Product_ClientA.name, Staging.Product_ClientA.description, 
	Staging.Product_ClientA.artNumber, Staging.Product_ClientA.price, 
	Staging.Product_ClientA.stock, Staging.Product_ClientA.deleted, Staging.Product_ClientA.brand_name
FROM Staging.Product_ClientA;

INSERT INTO Product(name, description, art_number, price, stock, is_deleted, brand)
	SELECT Staging.Product_ClientB.nama, Staging.Product_ClientB.deskripsi, 
	Staging.Product_ClientB.nomor_artikel, Staging.Product_ClientB.harga, 
	Staging.Product_ClientB.persediaan_barang, Staging.Product_ClientB.dihapus, Staging.Product_ClientB.nama_merek
FROM Staging.Product_ClientB;
