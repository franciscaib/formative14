use production;

ALTER TABLE product 
DROP COLUMN brand;

ALTER TABLE product 
ADD COLUMN brand_id INT NOT NULL; 

CREATE TABLE Brand(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)
);