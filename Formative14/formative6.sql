DELETE FROM Production.Product;

ALTER TABLE Production.product ADD CONSTRAINT fk_brand_id 
FOREIGN KEY(brand_id) REFERENCES Production.Brand(id);

INSERT INTO Production.Brand(name) VALUES
('Faber Castel'),('Sinar Dunia'),('Luna'),('Detol'),
('Pepsodent'),('Aqua'),('Sprite'),('Pantene'),
('Standart'),('Sunlight'),('Fox'),('Tango'),
('Silver Queen'),('Biskuat'),('Sedap'),('Segitiga Biru'),
('Gas LPG'),('Influenza'),('Handsaplast'),('Boom');

INSERT INTO Production.Product(name, description, art_number, price, stock, is_deleted, brand_id)
SELECT Staging.Product_ClientA.name, Staging.Product_ClientA.description, Staging.Product_ClientA.artNumber,
Staging.Product_ClientA.price, Staging.Product_ClientA.stock, Staging.Product_ClientA.deleted, Production.Brand.id
FROM Staging.Product_ClientA
JOIN Production.Brand ON Production.Brand.name = Staging.Product_ClientA.brand_name;

INSERT INTO Production.Product(name, description, art_number, price, stock, is_deleted, brand_id)
SELECT Staging.Product_ClientB.nama, Staging.Product_ClientB.deskripsi, Staging.Product_ClientB.nomor_artikel,
Staging.Product_ClientB.harga, Staging.Product_ClientB.persediaan_barang, Staging.Product_ClientB.dihapus, Production.Brand.id
FROM Staging.Product_ClientB
JOIN Production.Brand ON Production.Brand.name = Staging.Product_ClientB.nama_merek;